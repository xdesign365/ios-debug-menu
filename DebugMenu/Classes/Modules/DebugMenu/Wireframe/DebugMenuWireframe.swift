//
//  DebugMenuWireframe.swift
//  Generated using vipergen
//
//  Created by Robert Wilson on 12/11/2019.
//  Copyright © 2019 xDesign. All rights reserved.
//

import Foundation
import UIKit

protocol IDebugMenuWireframe { }

class DebugMenuWireframe: IDebugMenuWireframe {
    
    weak var presenter: DebugMenuPresenter!
    weak var viewController: DebugMenuViewController!
    
}
