//
//  DebugMenuInteractor.swift
//  Generated using vipergen
//
//  Created by Robert Wilson on 12/11/2019.
//  Copyright © 2019 xDesign. All rights reserved.
//

import Foundation

protocol DebugMenuInteractorInput: class { }

class DebugMenuInteractor {
    
    weak var presenter: DebugMenuInteractorOutput!
    
}

extension DebugMenuInteractor: DebugMenuInteractorInput { }
