//
//  DebugMenuViewController.swift
//  Generated using vipergen
//
//  Created by Robert Wilson on 12/11/2019.
//  Copyright © 2019 xDesign. All rights reserved.
//

import Foundation
import UIKit

protocol DebugMenuViewInput: class { }

class DebugMenuViewController: UIViewController {
    
    var eventHandler: DebugMenuViewOutput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}

extension DebugMenuViewController: DebugMenuViewInput { }
