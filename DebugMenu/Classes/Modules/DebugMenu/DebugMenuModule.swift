//
//  DebugMenuModuleBuilder.swift
//  Generated using vipergen
//
//  Created by Robert Wilson on 12/11/2019.
//  Copyright © 2019 xDesign. All rights reserved.
//

import Foundation
import UIKit

struct DebugMenuModule {

    let viewController: DebugMenuViewController
    let wireframe: DebugMenuWireframe
    
    static func build() -> DebugMenuModule {
        let viewController = DebugMenuViewController(nibName: nil, bundle: nil)
        let wireframe = DebugMenuWireframe()
        let interactor = DebugMenuInteractor()
        let presenter = DebugMenuPresenter(wireframe: wireframe,
                                            interactor: interactor,
                                            userInterface: viewController)
        
        wireframe.presenter = presenter
        wireframe.viewController = viewController
        
        interactor.presenter = presenter
        
        viewController.eventHandler = presenter
        
        return DebugMenuModule(viewController: viewController, wireframe: wireframe)
    }
    
}
