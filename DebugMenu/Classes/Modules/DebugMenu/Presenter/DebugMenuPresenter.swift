//
//  DebugMenuPresenter.swift
//  Generated using vipergen
//
//  Created by Robert Wilson on 12/11/2019.
//  Copyright © 2019 xDesign. All rights reserved.
//

import Foundation

protocol DebugMenuViewOutput: class { }

protocol DebugMenuInteractorOutput: class { }

class DebugMenuPresenter {
    
    private let wireframe: IDebugMenuWireframe
    private let interactor: DebugMenuInteractorInput
    private weak var userInterface: DebugMenuViewInput!
    
    init(wireframe: IDebugMenuWireframe, interactor: DebugMenuInteractorInput, userInterface: DebugMenuViewInput) {
        self.wireframe = wireframe
        self.interactor = interactor
        self.userInterface = userInterface
    }
    
}

// MARK: - DebugMenuViewOutput

extension DebugMenuPresenter: DebugMenuViewOutput { }

// MARK: - DebugMenuInteractorOutput

extension DebugMenuPresenter: DebugMenuInteractorOutput { }
