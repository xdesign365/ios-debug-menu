Pod::Spec.new do |s|

s.platform = :ios
s.ios.deployment_target = '12.0'
s.name = "DebugMenu"
s.summary = "DebugMenu lets developers configure different project settings from a debug window."
s.requires_arc = true

s.version = "0.1.4"

s.license = { :type => "MIT", :file => "LICENSE" }

s.author = { "Rob Wilson" => "robert.wilson@xdesign.com" }

s.homepage = "https://bitbucket.org/xdesign365/ios-debug-menu/src/master/"

s.source = { :git => "git@bitbucket.org:xdesign365/ios-debug-menu.git",
             :tag => "#{s.version}" }

s.framework = "UIKit"
s.dependency 'TinyConstraints'

s.source_files = "DebugMenu/**/*.swift"

# s.resources = "DebugMenu/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"

s.swift_version = "5.0"

end
